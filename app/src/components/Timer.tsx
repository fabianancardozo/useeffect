import { useEffect, useRef, useState } from "react";

type TimerArguments = {
    milisegundos: number
    segundos?: number,
}

export const Timer = ({milisegundos}: TimerArguments) => {

    const [segundos, setSegundos] = useState(0);
    const ref = useRef<NodeJS.Timeout>();
    

    useEffect( () => {
        
        //si ref.current existe, limpia el intervalo de ref.current
        ref.current && clearInterval(ref.current);
        
        ref.current = setInterval( () => setSegundos(s => s + 1) , milisegundos );
    }, [milisegundos])

  return (
    <div>
        <h4>Timer: <small>{segundos}</small></h4>
    </div>
    );
};

//useEffect accede al ciclo de vida del componente (funciona como componentDidMount)
//useEffect es una funcion. Recibe dos argumentos: un callback que define el efecto deseado 
//y un array de valores que definen las dependencias del efecto, 
//estas dependencias sirven para saber cuándo o más bien por qué el efecto debe ejecutarse.

//crea una referencia que no importa cuantas veces se reconstruya el componente siempre va a ser el mismo puntero en memoria


//en este caso mando a llamar a ref, seguido de setInterval, que es una funcion interna de JS, para hacer un intervalo de tiempo
//este setInverval, recibe el callback que indica la cantidad de milecimas de segundo que quiero contar
//el callback llama a 'setSegundos', que es la instruccion que tenemos para cambiar el 'segundo'
//si ponemos una funcion dentro de  setState lo primero que se emite es el estado actual