import './App.css';
import { TimerPadre } from './components/TimerPadre';

function App() {
  return (
    <div className="App">
      <header className="App-header">
        <h2>useEffect - useRef</h2>
        <hr/>
        <TimerPadre/>
      </header>
    </div>
  );
}

export default App;
